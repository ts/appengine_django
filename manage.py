#!/usr/bin/env python2.7

import logging
logging.basicConfig(
    level=logging.DEBUG,
    format='%(levelname)-8s %(asctime)s %(filename)s:%(lineno)s] %(message)s')

from appengine_django import InstallAppengineHelperForDjango,ModifyAvailableCommands,LoadDjango,LoadSdk
#InstallAppengineHelperForDjango()
LoadSdk()
LoadDjango()
ModifyAvailableCommands()


from django.core.management import execute_manager
try:
    import settings # Assumed to be in the same directory.
except IOError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)

if __name__ == "__main__":
    execute_manager(settings)
