'''
Created on Apr 5, 2011

@author: taras
'''
from itertools import chain

from django.forms.widgets import Widget, RadioInput, HiddenInput
from django.forms.util import flatatt
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode, StrAndUnicode
from django.utils.html import escape, conditional_escape
from django.forms.fields import ChoiceField
from django.forms.fields import MultipleChoiceField
import logging

class CPKChoiceField(ChoiceField):
    '''Custom PK choice field.
    Requires a model argument with a model responding to .get_by_pk(pk)
    '''
    
    def __init__(self, model=None, *args, **kwargs):
        super(CPKChoiceField, self).__init__(*args, **kwargs)
        self.model = model

    
    def valid_value(self, value):
        "Check to see if the provided value is a valid choice"
        return self.model.get_by_pk(value) != None

class MultipleTextField(MultipleChoiceField):
    '''Maps to a list property of strings
    Requires a model argument with a model responding to .get_by_pk(pk)
    '''
    
    def __init__(self, type=None, *args, **kwargs):
        super(MultipleTextField, self).__init__(*args, **kwargs)
        self._type = type

    
    def valid_value(self, value):
        "Check to see if the provided value is a valid choice"
        logging.debug('Value:'+value)
        return getattr(self._type,'valid_value',lambda x: True)(value) == True

    
class CPKMultipleChoiceField(MultipleChoiceField):
    '''Custom PK choice field.
    Requires a model argument with a model responding to .get_by_pk(pk)
    '''
    
    def __init__(self, model=None, *args, **kwargs):
        super(CPKMultipleChoiceField, self).__init__(*args, **kwargs)
        self.model = model

    
    def valid_value(self, value):
        "Check to see if the provided value is a valid choice"
        return self.model.get_by_pk(value) != None
    
class MultipleHiddenObject(HiddenInput):
    
    def render(self, name, value, attrs=None):
        if value is None:
            value = ['']

        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)

        results=''
        for val in value:
            if value != '':
                # Only add the 'value' attribute if a value is non-empty.
                final_attrs['value'] = force_unicode(self._format_value(val))
            results+=( u'<input%s />' % flatatt(final_attrs) )
        return mark_safe(results)
    
class FlyOutRadioFieldRenderer(StrAndUnicode):
    """
    An object used by RadioSelect to enable customization of radio widgets.
    """

    def __init__(self, name, value, attrs, choices):
        self.name, self.value, self.attrs = name, value, attrs
        self.choices = choices
    
    def render_choice(self,choice):
        self.idx+=1
        if len(choice)<3 or choice[2] == None or len(choice[2])==0:
            return RadioInput(self.name, self.value, self.attrs.copy(), choice, self.idx)
          
        return mark_safe(u'%s<ul>\n%s\n</ul>' % (
                RadioInput(self.name, self.value, self.attrs.copy(), choice, self.idx),
                u'\n'.join([u'<li>%s</li>' % self.render_choice(c) for c in choice[2]])
                ))

    def __unicode__(self):
        return self.render()
    
    def render_level(self,choices=()):
        self.idx=0;
        return mark_safe(u'<ul class="%s">\n%s\n</ul>' % ( self.attrs.pop('class'), u'\n'.join([u'<li>%s</li>'
                % self.render_choice(c) for c in choices])))
        

    def render(self):
        """Outputs a <ul> for this set of radio fields."""
        return self.render_level(self.choices)

class FlyOutSelector(Widget):
    """
    FlyOut Selector that is similar to <select><option>.
    """
    def __init__(self, attrs=None, choices=()):
        super(FlyOutSelector, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)
        
    def render(self, name, value, attrs=None, choices=()):
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs)
        output = [u'<ul class="flyout" %s>' % flatatt(final_attrs)]
        options = self.render_options(name, choices, [value])
        if options:
            output.append(options)
        output.append(u'</ul>')
        return mark_safe(u'\n'.join(output))

    def render_option(self, name, selected_choices, option_value, option_label):
        option_value = force_unicode(option_value)
        selected_html = (option_value in selected_choices) and u' selected="selected"' or ''
        return u'<input type="radio" name="%s" value="%s"%s>%s</option>' % (
            name,
            escape(option_value), selected_html,
            conditional_escape(force_unicode(option_label)))

    def render_options(self, name, choices, selected_choices):
        # Normalize to strings.
        selected_choices = set([force_unicode(v) for v in selected_choices])
        output = []
        for option_value, option_label in chain(self.choices, choices):
            if isinstance(option_label, (list, tuple)):
                output.append(u'<optgroup label="%s">' % escape(force_unicode(option_value)))
                for option in option_label:
                    output.append(self.render_option(selected_choices, *option))
                output.append(u'</optgroup>')
            else:
                output.append(self.render_option(selected_choices, option_value, option_label))
        return u'\n'.join(output)