"""
See http://docs.djangoproject.com/en/dev/ref/templates/api/#using-an-alternative-template-language

Use:
 * {{ url_for('view_name') }} instead of {% url view_name %},
 * <input type="hidden" name="csrfmiddlewaretoken" value="{{ csrf_token }}">
   instead of {% csrf_token %}.

"""

from django.template.loader import BaseLoader
from django.template.loaders.app_directories import app_template_dirs
from django.template import TemplateDoesNotExist
from django.core import urlresolvers
from django.conf import settings
import jinja2

class Template(jinja2.Template):
    def render(self, context):
        # flatten the Django Context into a single dictionary.
        context_dict = {}
        for d in context.dicts:
            context_dict.update(d)
        return super(Template, self).render(context_dict)

def configure_jinja2_env(env):
    env.template_class = Template

    # These are available to all templates.
    env.globals['url_for'] = urlresolvers.reverse
    env.globals['MEDIA_URL'] = settings.MEDIA_URL
    env.globals['settings'] = settings
    #env.globals['STATIC_URL'] = settings.STATIC_URL
    global_imports = getattr(settings, 'JINJA_GLOBALS', ())
    for imp in global_imports:
        method = get_callable(imp)
        method_name = getattr(method,'jinja_name',None)
        if not method_name == None:
            env.globals[method_name] = method
        else:
            env.globals[method.__name__] = method
            
    global_filters = getattr(settings, 'JINJA_FILTERS', ())
    for imp in global_filters:
        method = get_callable(imp)
        method_name = getattr(method,'jinja_name',None)
        if not method_name == None:
            env.filters[method_name] = method
        else:
            env.filters[method.__name__] = method
    
    global_tests = getattr(settings, 'JINJA_TESTS', ())
    for imp in global_tests:
        method = get_callable(imp)
        method_name = getattr(method,'jinja_name',None)
        if not method_name == None:
            env.tests[method_name] = method
        else:
            env.tests[method.__name__] = method

class Loader(BaseLoader):
    is_usable = True
    
    jinja_global_exts = getattr(settings, 'JINJA_EXTS', ())
    jinja_env_opts = getattr(settings, 'JINJA_ENV', {})
    
    env = jinja2.Environment(loader=jinja2.FileSystemLoader(app_template_dirs),
                             extensions=jinja_global_exts,
                             **jinja_env_opts
                             )
    configure_jinja2_env(env)

    def load_template(self, template_name, template_dirs=None):
        try:
            template = self.env.get_template(template_name)
        except jinja2.TemplateNotFound:
            raise TemplateDoesNotExist(template_name)
        return template, template.filename
