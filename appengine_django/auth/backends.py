import logging,sys
from django.contrib.auth.models import User
from django.conf import settings

class ModelBackend(object):
  """
  Authenticates against user.
  """
  supports_object_permissions = False
  supports_anonymous_user = True
  auth_field= getattr(settings,'AUTH_USER_LOGIN','username')

  # TODO: Model, login attribute name and password attribute name should be
  # configurable.
  def authenticate(self, username=None, password=None):
    logging.debug('auth user:'+username)

    try:
      user = User.all().filter(self.auth_field+' =',username).get()
      if not user:
        logging.warn('Unknown user:'+username)
        return None
      if user.check_password(password):
        return user
      logging.warn('Invalid Password for:'+username)            
      return None
    except:
      logging.exception("Unexpected error")
      return None

  def get_group_permissions(self, user_obj):
      """
      Returns a set of permission strings that this user has through his/her
      groups.
      """
      if not hasattr(user_obj, '_group_perm_cache'):
          if user_obj.is_superuser:
              perms = Permission.objects.all()
          else:
              perms = Permission.objects.filter(group__user=user_obj)
          perms = perms.values_list('content_type__app_label', 'codename').order_by()
          user_obj._group_perm_cache = set(["%s.%s" % (ct, name) for ct, name in perms])
      return user_obj._group_perm_cache

  def get_all_permissions(self, user_obj):
      if user_obj.is_anonymous():
          return set()
      if not hasattr(user_obj, '_perm_cache'):
          user_obj._perm_cache = set([u"%s.%s" % (p.content_type.app_label, p.codename) for p in user_obj.user_permissions.select_related()])
          user_obj._perm_cache.update(self.get_group_permissions(user_obj))
      return user_obj._perm_cache

  def has_perm(self, user_obj, perm):
      return perm in self.get_all_permissions(user_obj)

  def has_module_perms(self, user_obj, app_label):
      """
      Returns True if user_obj has any permissions in the given app_label.
      """
      for perm in self.get_all_permissions(user_obj):
          if perm[:perm.index('.')] == app_label:
              return True
      return False

  def get_user(self, user_id):
      try:
          return User.get_by_id(user_id)
      except:
          return None