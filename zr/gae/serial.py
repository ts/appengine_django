'''
Created on Mar 30, 2011

@author: taras
'''

import json
from google.appengine.ext import db

def as_dict_encoder(o):
  if isinstance(o,GAESerializable):
    return o.as_dict()
  if isinstance(o,db.Key):
    return str(o)

  raise TypeError(repr(o) + " is not JSON serializable")


class GAESerializable(object):
    
    PK_DELIM = '_'

    @property
    def pk(self):
        return self.pk_from_key(self.key())
      
    @property
    def key_name(self):
      return self.key().id_or_name()
    
    @classmethod
    def pk_from_key(cls,key):
      return str(key)

    @classmethod
    def key_for_pk(cls,pk):
      return db.Key(encoded=pk)
      
    @classmethod
    def get_by_pk(cls,pk):
      return cls.get(cls.key_for_pk(pk))

    def meta_properties(self):
      '''Every value returned will be added to the dict.
      Value will be called as a property
      '''
      meta = getattr(self.__class__, 'Meta', None)

      return getattr(meta,'serialize_fields',()) + ('pk',)

    def remap_properties(self):
      meta = getattr(self.__class__, 'Meta', None)

      return getattr(meta,'remap_properties',{})
    
    def as_dict(self,only=None):
        vals={}
        remap=self.remap_properties()
        for key in (only or (list(getattr(self,'properties',lambda :{})().keys()) + list(self.meta_properties()))):
            val=getattr(self,remap.get(key,key))
            if isinstance(val,GAESerializable):
              #TODO: make async and/or on demand
              vals[key]=val.as_dict()
            if isinstance(val,db.GeoPt):
              vals[key]=str(val)
            else:
              vals[key]=val
        return vals
    
    def as_json(self):
        return json.dumps(self.as_dict(), ensure_ascii=False)
    
class DisplayRequirement(unicode,GAESerializable):
    
  def __init__(self, field, test=None, values=None):
    super(DisplayRequirement, self).__init__()
    if test==None and isinstance(field,unicode):
      field=json.loads(field)
    if test is None and isinstance(field,dict):
      values=field.get('values',[])
      test=field.get('test',None)
      field=field.get('field',None)
      
    if test is None:
      raise datastore_errors.BadValueError(
        'Expected dict with {"field","test","values"} or individual arguments received %s' %
        repr((field,test,value))
      )

    self._field = field
    self._test = test
    self._values = values
    
  @classmethod
  def properties(cls):
      return {'field':unicode,'test':unicode,'values':list}
  
  @property
  def field(self):
      return self._field
  
  @property
  def test(self):
      return self._test

  @property
  def values(self):
      return self._values
    
  def __repr__(self):
    """Returns an eval()able string representation of this IM.

    The returned string is of the form:

      formquest.DisplayRequirement('pk', [values])

    Returns:
      string
    """
    return 'formquest.DisplayRequirement(%r,%r,%r)' % (self._field, self._test, self._values)

  def as_dict(self):
    return {'field':self._field,'test':self._test,'values':self._values}

  def __unicode__(self):
    return u'{"field":"%s","test":"%s","values":["%s"]}' % (self._field, self._test, '","'.join(self._values))
  
  __str__ = __unicode__

  def ToXml(self):
    return (u'<formquest:DispReq field=%s test=%s values=%s />' %
            (saxutils.quoteattr(self._field),
             saxutils.quoteattr(self._test),
             saxutils.quoteattr('|'.join(self._values))))

  def __len__(self):
    return len(unicode(self))


class DisplayRequirementProperty(db._CoercingProperty):
  
  def __get__(self, model_instance, model_class):
    """Get DisplayRequirement object.

    Returns:
      DisplayRequirement object if property is set, else None.

    """
    if model_instance is None:
      return self

    if hasattr(model_instance, self.__id_attr_name()):
      serial_val = getattr(model_instance, self.__id_attr_name())
    else:
      serial_val = None
    if serial_val is not None:
      resolved = getattr(model_instance, self.__resolved_attr_name())
      if resolved is None:
        resolved = self.validate(reference_id)
        setattr(model_instance, self.__resolved_attr_name(), resolved)
      return resolved
    else:
      return None

  def __set__(self, model_instance, value):
    """Set ether serialized or actual value."""

    value = self.validate(value)
    if value is not None:
      if isinstance(value, self.data_type):
        setattr(model_instance, self.__id_attr_name(), str(value))
        setattr(model_instance, self.__resolved_attr_name(), value)
    else:
      setattr(model_instance, self.__id_attr_name(), None)
      setattr(model_instance, self.__resolved_attr_name(), None)

  def get_value_for_datastore(self, model_instance):
    """Get key of reference rather than reference itself."""

    return getattr(model_instance, self.__id_attr_name())
  
  def get_updated_value_for_datastore(self, model_instance):
    """Determine new value if updated.

    Returns:
      Datastore representation of the new model value in a form that is
      appropriate for storing in the datastore, or AUTO_UPDATE_UNCHANGED.
    """
    resolved = getattr(model_instance, self.__resolved_attr_name(), None)
    if resolved:
      new_val = str(resolved)
      old_val = getattr(model_instance, self.__id_attr_name(), None)
      if new_val != old_val:
        setattr(model_instance, self.__id_attr_name(), new_val)
        return new_val
    return db.AUTO_UPDATE_UNCHANGED
  
  def __id_attr_name(self):
    """Get attribute of referenced id.

    Returns:
      Attribute where to store id of referenced entity.
    """
    return self._attr_name()

  def __resolved_attr_name(self):
    """Get attribute of resolved attribute.

    Returns:
      Attribute name of where to store resolved object instance.
    """
    return '_RESOLVED' + self._attr_name()
    
  data_type = DisplayRequirement
    
1