# Copyright 2008 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Explicitly set the name of this package to "appengine".
#
# The rationale for this is so that Django can refer to the database as
# "appengine" even though at a filesystem level it appears as the "db" package
# within the appengine_django package.
import logging
import os

def get_datastore_paths(conf):
  """Returns a tuple with the path to the datastore and history file.

  The datastore is stored in the same location as dev_appserver uses by
  default, but the name is altered to be unique to this project so multiple
  Django projects can be developed on the same machine in parallel.

  Returns:
    (datastore_path, history_path)
  """
  if conf:
    datastore_path=conf.get('datastore_path')
    history_path=conf.get('history_path')
    if datastore_path and history_path:
      logging.debug('Using custom datastore path:'+
                    conf['datastore_path']+" history:"+conf['history_path'])
      return (datastore_path,history_path)
  from google.appengine.tools import dev_appserver_main
  datastore_path = dev_appserver_main.DEFAULT_ARGS['datastore_path']
  history_path = dev_appserver_main.DEFAULT_ARGS['history_path']
  datastore_path = datastore_path.replace("dev_appserver", "django_%s" % appid)
  history_path = history_path.replace("dev_appserver", "django_%s" % appid)
  return datastore_path, history_path


def get_test_datastore_paths(inmemory=True,conf=None):
  """Returns a tuple with the path to the test datastore and history file.

  If inmemory is true, (None, None) is returned to request an in-memory
  datastore. If inmemory is false the path returned will be similar to the path
  returned by get_datastore_paths but with a different name.

  Returns:
    (datastore_path, history_path)
  """
  if inmemory:
    return None, None
  datastore_path, history_path = get_datastore_paths(conf)
  datastore_path = datastore_path+ "-test"
  history_path = history_path+"-test"
  return datastore_path, history_path


def destroy_datastore(datastore_path, history_path):
  """Destroys the appengine datastore at the specified paths."""
  for path in [datastore_path, history_path]:
    if not path: continue
    try:
      os.remove(path)
    except OSError, e:
      if e.errno != 2:
        logging.error("Failed to clear datastore: %s" % e)
        
  
__name__ = "appengine"